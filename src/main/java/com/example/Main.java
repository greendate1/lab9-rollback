package com.example

import java.util.LinkedList;
import java.util.Arrays;
import java.util.List;
import java.sql.*;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "admin";
    static final String PASS = "admin";

    public static void main(String[] args) {

      LinkedList<Integer> ids = new LinkedList<Integer>(List.of(1, 2, 3));
      LinkedList<Integer> empIds = new LinkedList<Integer>(List.of(106, 107, 108));
      LinkedList<Integer> amounts = new LinkedList<Integer>(List.of(12000, 24000, 36000));

      add_salaries(ids, empIds, amounts);
    }


    public static void printRs(ResultSet rs) throws SQLException{
        //Ensure we start with first row
        rs.beforeFirst();
        while(rs.next()){
            //Retrieve by column name
            int id  = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("first");
            String last = rs.getString("last");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Age: " + age);
            System.out.print(", First: " + first);
            System.out.println(", Last: " + last);
        }
        System.out.println();
    }

    private static void add_salaries(LinkedList<Integer> ids, LinkedList<Integer> employee_ids, LinkedList<Integer> amount) {
          Connection conn = null;
          Statement stmt = null;

          try {

              Class.forName("org.postgresql.Driver");
              System.out.println("Connecting to database...");
              conn = DriverManager.getConnection(DB_URL, USER, PASS);
              conn.setAutoCommit(false);

              System.out.println("Creating statement...");
              stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

              for (int i = 0; i < ids.size(); i++) {
                  // Check that there is no such employeeId yet
                  String SQL = "SELECT * FROM Salary s WHERE s.employee_id=" + employee_ids.get(i);
                  ResultSet res = stmt.executeQuery(SQL);
                  if (res.next()) throw new SQLException();

                  SQL = "INSERT INTO Salary VALUES (" +
                          ids.get(i) + "," +
                          employee_ids.get(i) + "," +
                          amount.get(i) + ")";
                  stmt.executeUpdate(SQL);
              }

              // Commit outside of loop will ensure that the whole
              // transaction will be cancelled in case of rollback
              conn.commit();

              stmt.close();
              conn.close();

          } catch (SQLException | IndexOutOfBoundsException se) {

              se.printStackTrace();
              System.out.println("Rolling back data here...");
              try {
                  if (conn != null) conn.rollback();
              } catch (SQLException se2) {
                  se2.printStackTrace();
              }

          } catch (Exception e) {
              e.printStackTrace();

          } finally {
              try {
                  if (stmt != null) stmt.close();
              } catch (SQLException ignored) {}

              try {
                  if (conn != null) conn.close();
              } catch (SQLException se) {
                  se.printStackTrace();
              }
          }
      }

}
